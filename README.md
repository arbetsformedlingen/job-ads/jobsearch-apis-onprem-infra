# Configuration for deployment of JobSearch API in onprem openshift clusters


## Openshift Secrets that must be created:
`db-account-api`

DB_USER     username for opensearch with read access
DB_PWD      password
